package com.example.supermarket;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.supermarket.Tools.MesActivity;

import java.util.List;

public class Classification extends AppCompatActivity {
    private Button Button1;

    private Button Button3;


    private Button Button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classification);
        Intent intent = getIntent();
        LoginActivity.account = intent.getStringExtra("account");
        Button1 = findViewById(R.id.button1);

        Button3 = findViewById(R.id.button3);
        Button2 = findViewById(R.id.button2);
        Button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //创建Intent 对象
                Intent intent = new Intent(Classification.this, ListActivity.class);
                //启动Activity
                startActivity(intent);
            }
        });

        Button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //创建Intent 对象
                Intent intent = new Intent(Classification.this, MesActivity.class);
                //启动Activity
                startActivity(intent);
            }
        });


        Button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //创建Intent 对象
                Intent intent = new Intent(Classification.this,ShopActivity.class);
                //启动Activity
                startActivity(intent);
            }
        });
    }
}
