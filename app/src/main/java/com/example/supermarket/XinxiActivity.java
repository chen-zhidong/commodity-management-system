package com.example.supermarket;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class XinxiActivity extends AppCompatActivity {
    private Button btn_xl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xinxi);
        btn_xl = findViewById(R.id.btn_xl);
        btn_xl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //创建Intent 对象
                Intent intent = new Intent(XinxiActivity.this, Classification.class);
                //启动Activity
                startActivity(intent);
            }
        });
    }
}

