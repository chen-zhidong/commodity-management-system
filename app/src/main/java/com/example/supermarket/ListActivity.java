package com.example.supermarket;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.supermarket.Tools.DataDB;
import com.example.supermarket.Tools.DataTools;

public class ListActivity extends AppCompatActivity {
    private Button Button1;
    private Button Button2;
    private Button Button3;
    private Button Button4;


    DataDB db;
    DataTools d_tools;

    private Button nn_add,nnt_add,mt_add,qq_add;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        db = new DataDB(ListActivity.this,"Data.db",null,1);
        d_tools = new DataTools(db);
        Button1 = findViewById(R.id.button1);
        Button2 = findViewById(R.id.button2);
        Button3 = findViewById(R.id.button3);
        Button4 = findViewById(R.id.button4);
        nn_add = findViewById(R.id.nn_add);
        nnt_add = findViewById(R.id.nnt_add);
        mt_add = findViewById(R.id.mt_add);
        qq_add = findViewById(R.id.qq_add);
        Button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //创建Intent 对象
                Intent intent = new Intent(ListActivity.this, GuanliActivity.class);
                //启动Activity
                startActivity(intent);
            }
        });
        Button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //创建Intent 对象
                Intent intent = new Intent(ListActivity.this, GuanliActivity2.class);
                //启动Activity
                startActivity(intent);
            }
        });
        Button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //创建Intent 对象
                Intent intent = new Intent(ListActivity.this, GuanliActivity3.class);
                //启动Activity
                startActivity(intent);
            }
        });
        Button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //创建Intent 对象
                Intent intent = new Intent(ListActivity.this, GuanliActivity4.class);
                //启动Activity
                startActivity(intent);
            }
        });

        nnt_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (d_tools.select(LoginActivity.account,"椰子糖")){
                    String count = d_tools.select(LoginActivity.account,"椰子糖","count");
                    int i = 0;
                    if (count != null&&!count.equals("")){
                        i = Integer.parseInt(count);
                        i++;
                        d_tools.update(LoginActivity.account,"椰子糖",""+i,"count");
                        Toast.makeText(ListActivity.this,"加入成功",Toast.LENGTH_SHORT).show();
                    }
                }else {
                    d_tools.insert(LoginActivity.account,"椰子糖",
                            "7",
                            "1",
                            R.drawable.yz);
                    Toast.makeText(ListActivity.this,"加入成功",Toast.LENGTH_SHORT).show();
                }
            }
        });

        nn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (d_tools.select(LoginActivity.account,"特仑苏")){
                    String count = d_tools.select(LoginActivity.account,"特仑苏","count");
                    int i = 0;
                    if (count != null&&!count.equals("")){
                        i = Integer.parseInt(count);
                        i++;
                        d_tools.update(LoginActivity.account,"特仑苏",""+i,"count");
                        Toast.makeText(ListActivity.this,"加入成功",Toast.LENGTH_SHORT).show();
                    }
                }else {
                    d_tools.insert(LoginActivity.account,"特仑苏",
                            "7",
                            "1",
                            R.drawable.tls);
                    Toast.makeText(ListActivity.this,"加入成功",Toast.LENGTH_SHORT).show();
                }
            }
        });

        mt_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (d_tools.select(LoginActivity.account,"大白兔")){
                    String count = d_tools.select(LoginActivity.account,"大白兔","count");
                    int i = 0;
                    if (count != null&&!count.equals("")){
                        i = Integer.parseInt(count);
                        i++;
                        d_tools.update(LoginActivity.account,"大白兔",""+i,"count");
                        Toast.makeText(ListActivity.this,"加入成功",Toast.LENGTH_SHORT).show();
                    }
                }else {
                    d_tools.insert(LoginActivity.account,"大白兔",
                            "9",
                            "1",
                            R.drawable.dbt);
                    Toast.makeText(ListActivity.this,"加入成功",Toast.LENGTH_SHORT).show();
                }
            }
        });

        qq_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (d_tools.select(LoginActivity.account,"三只松鼠")){
                    String count = d_tools.select(LoginActivity.account,"三只松鼠","count");
                    int i = 0;
                    if (count != null&&!count.equals("")){
                        i = Integer.parseInt(count);
                        i++;
                        d_tools.update(LoginActivity.account,"三只松鼠",""+i,"count");
                        Toast.makeText(ListActivity.this,"加入成功",Toast.LENGTH_SHORT).show();
                    }
                }else {
                    d_tools.insert(LoginActivity.account,"三只松鼠",
                            "15",
                            "1",
                            R.drawable.sz);
                    Toast.makeText(ListActivity.this,"加入成功",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
