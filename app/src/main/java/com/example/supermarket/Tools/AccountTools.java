package com.example.supermarket.Tools;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class AccountTools {

    SQLiteDatabase db;

    public AccountTools(AccountDB db){
        this.db = db.getWritableDatabase();
    }

    //插入数据
    public void insert(String account,String password){
        ContentValues values = new ContentValues();
        values.put("account",account);
        values.put("passsword",password);
        db.insert("Account",null,values);
    }

    //查询数据
    public boolean select(String account,String password){
        boolean b = false;
        Cursor cursor = db.query("Account",null,null,null,null,null,null);
        if (cursor.moveToFirst()){
            do {
                b = false;
                String s_account = cursor.getString(cursor.getColumnIndex("account"));
                String s_password = cursor.getString(cursor.getColumnIndex("passsword"));
                if (s_account.equals(account)&&s_password.equals(password)){
                    b = true;
                    break;
                }
            }while (cursor.moveToNext());
        }
        return b;
    }

    public boolean select(String account){
        boolean b = false;
        Cursor cursor = db.query("Account",null,null,null,null,null,null);
        if (cursor.moveToFirst()){
            do {
                b = false;
                String s_account = cursor.getString(cursor.getColumnIndex("account"));
                if (s_account.equals(account)){
                    b = true;
                    break;
                }
            }while (cursor.moveToNext());
        }
        return b;
    }
}
