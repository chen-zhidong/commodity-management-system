package com.example.supermarket.Tools;

public class Tools {

    public int image;
    public String name;
    public String count;
    public String price;

    public Tools(String name,String count,int image,String price){
        this.count = count;
        this.image = image;
        this.name = name;
        this.price = price;
    }

    public Tools(String name,String price,int image){
        this.price = price;
        this.name = name;
        this.image =image;
    }
}
